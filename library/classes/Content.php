<?php
 /*
  * @class: Content
  * @description: Content populating functions
  * @author Nick Worth
  * @version: 1.0
  */

class Content{

  public function __construct(){

  }


  /**
   * GET POST TYPE OPTIONS
   * 
   * @author Nick Worth
   * @since 1.0
   * @return <array> $post_types
   */
  public function get_post_type_options(){  
    $post_types = get_post_types( '', 'names' );

    $type_exclude = array('attachment', 'revision', 'nav_menu_item', 'acf');
    foreach ($type_exclude as $value) {
        $options = array_search($value, $post_types );
        unset($post_types [$options]);
    }
    return $post_types;
  }

  /**
   * GET CATEGORIES ARRAY
   * 
   * @author Nick Worth
   * @since 1.0
   * @return <array> $post_types
   */
  public function get_categories_array(){  
    $categories = get_categories();
    $cat_array['All Category'] = "";
    foreach ($categories as $category) {
        $cat_array[$category->name] = $category->term_id;
    }
    return $cat_array;
  }

  /**
   * GET TAXONOMY SELECT/DROPDOWN
   * 
   * @author Nick Worth
   * @since 1.0
   * @param <string> $taxonomy
   * @param <string> $orderby
   * @param <string> $order
   * @param <string> $limit
   * @param <string> $name
   * @param <boolean> $show_option_all
   * @param <boolean> $show_option_none
   * @return <string>
   */
  public function get_taxonomy_dropdown($taxonomy, $args, $default = "", $onChange = null, $show_option_all = null, $show_option_none = null){  
      $terms = get_terms( $taxonomy, $args );
      if ( $terms ) {
        printf( '<select id="%s" name="%s" class="postform" onChange="'.$onChange.'">', esc_attr( $taxonomy ), esc_attr( $taxonomy ) );
        if ( $show_option_all ) {
          printf( '<option value="0">%s</option>', esc_html( $show_option_all ) );
        }
        if ( $show_option_none ) {
          printf( '<option value="-1">%s</option>', esc_html( $show_option_none ) );
        }
        foreach ( $terms as $term ) {
          if($default == esc_html( $term->slug )){
            printf( '<option selected="selected" value="%s">%s</option>', esc_attr( $term->slug ), esc_html( $term->name ) );
          } else {
            printf( '<option value="%s">%s</option>', esc_attr( $term->slug ), esc_html( $term->name ) );
          }
        }
        print( '</select>' );
      }
  }

  /**
   * GET IMAGES ARRAY
   * 
   * @author Nick Worth
   * @since 1.0
   * @return <array> $post_types
   */
  public function get_images_array(){  
    $images = get_intermediate_image_sizes();

    foreach ($images as $k => $v) {
        $image_array[ucfirst($v)] = $v;
    }
    return $image_array;
  }

  /**
   * SET EXCERPT LENGTH
   * 
   * @author Nick Worth
   * @since 1.0
   * @return <string>
   */
  public function the_excerpt_max_charlength($charlength,$more_link="[...]") {
    add_filter( 'excerpt_more', function(){ return ""; } );
    $excerpt = get_the_excerpt();
    $charlength++;
    $string = "";

     if ( mb_strlen( $excerpt ) > $charlength ) {
       $subex = mb_substr( $excerpt, 0, $charlength - 5 );
       $exwords = explode( ' ', $subex );
       $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
       if ( $excut < 0 ) {
           $string .= mb_substr( $subex, 0, $excut );
       } else {
           $string .= $subex;
       }
       $string .= '<a href="' . get_permalink() . '" class="read-more">'.$more_link.'</a>';
     } else {
         $string .= $excerpt;
     }

    return $string;
  }

  /**
   * SOCIAL SHARE ICONS
   * 
   * @author Nick Worth
   * @since 1.0
   * @param <array> icons
   * @return <string>
   */
  public function share_icons($icons) {
    foreach ($icons as $key => $icon) {
      switch ($icon) {
        case 'facebook':
          echo '<a class="social_icon '.$icon.'" title="'.$icon.'" href="http://facebook.com/"></a>';
          break;
        
        case 'twitter':
          echo '<a class="social_icon '.$icon.'" title="'.$icon.'" href="http://twitter.com/"></a>';
          break;

        case 'email':
          echo '<a class="social_icon '.$icon.'" title="'.$icon.'" href="mailto://'.get_field('email_link','options').'"></a>';          
          break;

        default:
          # code...
          break;
      }
    }
  }

}
?>