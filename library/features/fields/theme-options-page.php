<?php 
/*
Feature Name:   ACF Fields: Theme Options
Developer URI:  http://www.DivTruth.com
Description:    All fields displayed on the theme options page should be here
Version:        1.0
Dependencies:   ACF
*/

/*****************
 * FILTER OPTIONS
 *****************/
if(function_exists("register_field_group"))
{
        register_field_group(array (
                'id' => 'acf_filter-options',
                'title' => 'Filter Options',
                'fields' => array (
                        array (
                                'key' => 'field_51f43fa42fd56',
                                'label' => 'Widget Shortcodes',
                                'name' => 'widget_shortcodes',
                                'type' => 'radio',
                                'instructions' => 'Enable Shortcodes in widgets',
                                'choices' => array (
                                        'Enable' => 'Enable',
                                        'Disable' => 'Disable',
                                ),
                                'other_choice' => 0,
                                'save_other_choice' => 0,
                                'default_value' => 'Disable',
                                'layout' => 'horizontal',
                        ),
                        array (
                                'key' => 'field_51f43fa4ahhadd5',
                                'label' => 'Twitter Handlers',
                                'name' => 'twitter_handlers',
                                'type' => 'radio',
                                'instructions' => 'Link Twitter Handlers in content',
                                'choices' => array (
                                        'Enable' => 'Enable',
                                        'Disable' => 'Disable',
                                ),
                                'other_choice' => 0,
                                'save_other_choice' => 0,
                                'default_value' => 'Disable',
                                'layout' => 'horizontal',
                        ),
                ),
                'location' => array (
                        array (
                                array (
                                        'param' => 'options_page',
                                        'operator' => '==',
                                        'value' => 'acf-options-theme-settings',
                                        'order_no' => 5,
                                        'group_no' => 0,
                                ),
                        ),
                ),
                'options' => array (
                        'position' => 'normal',
                        'layout' => 'default',
                        'hide_on_screen' => array (
                        ),
                ),
                'menu_order' => 5,
        ));
}

/**************************
 * Social Network Settings
 **************************/
if(function_exists("register_field_group")){
        register_field_group(array (
                'id' => 'acf_status-options',
                'title' => 'Status Options',
                'fields' => array (
                        array (
                                'key' => 'field_51aa84a34ahadd7',
                                'label' => 'Theme Develop Mode',
                                'name' => 'develop_mode',
                                'type' => 'radio',
                                'instructions' => 'Is theme currently in development (error message will appear while in development)',
                                'choices' => array (
                                        'Enable' => 'Enable',
                                        'Disable' => 'Disable',
                                ),
                                'other_choice' => 0,
                                'save_other_choice' => 0,
                                'default_value' => 'Enable',
                                'layout' => 'horizontal',
                        ),
                ),
                'location' => array (
                        array (
                                array (
                                        'param' => 'options_page',
                                        'operator' => '==',
                                        'value' => 'acf-options-theme-settings',
                                        'order_no' => 0,
                                        'group_no' => 0,
                                ),
                        ),
                ),
                'options' => array (
                        'position' => 'side',
                        'layout' => 'default',
                        'hide_on_screen' => array (
                        ),
                ),
                'menu_order' => 0,
        ));
}


/********************************
 * Twitter API 1.1 Authentication
 ********************************/
if(function_exists("register_field_group")){
        register_field_group(array (
                'id' => 'acf_twitter-authentication',
                'title' => 'Twitter Authentication',
                'fields' => array (
                        array (
                                'key' => 'field_5227ae62169ed',
                                'label' => 'Oauth Access Token',
                                'name' => 'oauth_access_token',
                                'type' => 'text',
                                'instructions' => 'Enter Access Token. <a href="https://dev.twitter.com/docs/auth/tokens-devtwittercom">Need Help?</a>',
                                'default_value' => '',
                                'placeholder' => '3712828794-aagRVUk9jMa8XhRnA9ebvUiraXym1dGXqodHzF3',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'none',
                                'maxlength' => 50,
                        ),
                        array (
                                'key' => 'field_5227aff1169ee',
                                'label' => 'Oauth Access Token Secret',
                                'name' => 'oauth_access_token_secret',
                                'type' => 'text',
                                'instructions' => 'Enter Access Token Secret. <a href="https://dev.twitter.com/docs/auth/tokens-devtwittercom">Need Help?</a>',
                                'default_value' => '',
                                'placeholder' => 'LnbIzxJdLIrbVz6IM7ubmI1iJLMUy3sRTumQYWbIe',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'none',
                                'maxlength' => 41,
                        ),
                        array (
                                'key' => 'field_5227b087169ef',
                                'label' => 'Consumer Key',
                                'name' => 'consumer_key',
                                'type' => 'text',
                                'instructions' => 'Enter Consumer Key. <a href="http://www.youtube.com/watch?v=5PUC9yGS4RI">Need Help?</a>',
                                'default_value' => '',
                                'placeholder' => 'MItaitONxu3R3pu50U0B',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'none',
                                'maxlength' => '',
                        ),
                        array (
                                'key' => 'field_5227b0e2169f0',
                                'label' => 'Consumer Secret',
                                'name' => 'consumer_secret',
                                'type' => 'text',
                                'instructions' => 'Enter Consumer Secret. <a href="http://www.youtube.com/watch?v=5PUC9yGS4RI">Need Help?</a>',
                                'default_value' => '',
                                'placeholder' => 'pt8Uu39wyrpBZMZOHVGeIaaPGkzIxjNv1WGT2Te24B',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'none',
                                'maxlength' => '',
                        ),
                ),
                'location' => array (
                        array (
                                array (
                                        'param' => 'options_page',
                                        'operator' => '==',
                                        'value' => 'acf-options-theme-settings',
                                        'order_no' => 0,
                                        'group_no' => 0,
                                ),
                        ),
                ),
                'options' => array (
                        'position' => 'side',
                        'layout' => 'default',
                        'hide_on_screen' => array (
                        ),
                ),
                'menu_order' => 2,
        ));
}

/**************************
 * Social Network Settings
 **************************/
if(function_exists("register_field_group")){
        register_field_group(array (
                'id' => 'acf_media-options',
                'title' => 'Media Options',
                'fields' => array (
                        array (
                                'key' => 'field_51a4dfa4ahadd7',
                                'label' => 'JPG Compression Quality',
                                'name' => 'jpg_quality',
                                'type' => 'text',
                                'instructions' => 'Set the compression percentage rate for JPG uploaded images',                                
                                'default_value' => '90',
                                'formatting' => 'none',
                        ),
                ),
                'location' => array (
                        array (
                                array (
                                        'param' => 'options_page',
                                        'operator' => '==',
                                        'value' => 'acf-options-theme-settings',
                                        'order_no' => 0,
                                        'group_no' => 0,
                                ),
                        ),
                ),
                'options' => array (
                        'position' => 'side',
                        'layout' => 'default',
                        'hide_on_screen' => array (
                        ),
                ),
                'menu_order' => 1,
        ));
}
?>