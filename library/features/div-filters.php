<?php
/**
 * Author: Div Truth
 * Description: Filters used by Div Truth Starter Theme
 */

add_action('init','div_after_init_filters');
function div_after_init_filters(){
  /**
   * SHORTCODES IN WIDGET FILTER
   * @author: Nick Worth
   * @since: 1.0
   */
  if(get_field('widget_shortcodes','option')){
    add_filter( 'widget_text', 'do_shortcode');
  }

  /**
   * TWITTER HANDLER FILTER
   * Automatically link Twitter usernames in WordPress
   *
   * @author: Nick Worth
   * @since: 1.0
   */
  function twtreplace($content) {
    $twtreplace = preg_replace('/([^a-zA-Z0-9-_&])@([0-9a-zA-Z_]+)/','$1<a href="http://twitter.com/$2" target="_blank" rel="nofollow">@$2</a>',$content);
    return $twtreplace;
  }
  if(get_field('twitter_handlers','option')){
    add_filter('the_content', 'twtreplace');
    add_filter('comment_text', 'twtreplace');
  }

  /**
   * JPG COMPRESSION FILTER
   * Set WP compression quality
   *
   * @author: Nick Worth
   * @since: 1.0
   */
  function div_compress_images($quality) {
    return $quality;
  }
  if(get_field('jpg_quality','option')){
    add_filter('jpeg_quality', 'div_compress_images');
  }
}

/**
 * CUSTOM EXCERPT LENGTH
 * Change the default WordPress excerpt length
 *
 * @author Nick Worth
 * @since 1.0
 * @param $length (string): 
 * @return <number> (default 28)
 */
function div_excerpt( $length ) {
    if($length)
      return $length;
    else 
      return 28;
}
add_filter( 'excerpt_length', 'div_excerpt', 999 );

/**
 * ALLOW PHP IN WIDGET
 * @author: Betty Urquida
 * @since: 1.0
 */
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}
add_filter('widget_text','execute_php',100);

/**
 * DIV PREV/NEXT FILTER
 * Custom prev/next options
 *
 * @author: Nick Worth
 * @since: 1.0
 */
// add_filter('wp_link_pages_args', 'wp_link_pages_args_prevnext_add');
function wp_link_pages_args_prevnext_add($args){
    global $page, $numpages, $more, $pagenow;

    if (!$args['next_or_number'] == 'next_and_number') 
        return $args; # exit early

    $args['next_or_number'] = 'number'; # keep numbering for the main part
    if (!$more)
        return $args; # exit early

    if($page-1) # there is a previous page
        $args['before'] .= _wp_link_page($page-1)
            . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a>'
        ;

    if ($page<$numpages) # there is a next page
        $args['after'] = _wp_link_page($page+1)
            . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
            . $args['after']
        ;

    return $args;
}

/**
 * GET LIST FROM DIRECTORY
 * Adds "parent" class to menus
 *
 * @author: Nick Worth
 * @since: 1.0.1.0
 */
function theme_add_menu_parent_classes( $classes, $item, $args ) {
    $children = get_posts( array(
        'meta_query' => array (
            array(
                'key' => '_menu_item_menu_item_parent',
                'value' => $item->ID )
        ),
        'post_type' => $item->post_type ) );
    if (count($children) > 0) {
        array_push($classes,'parent'); // add the class .parent to the current menu item
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'theme_add_menu_parent_classes', 10, 3 );

/**
 * MAINTAIN G-D FILTER
 * Wraps all usage of G-d with <nobr>
 *
 * @author: Nick Worth
 * @since: 1.0
 */
function G_Dfilter($content) {
  $G_Dfilter = preg_replace('/\bG-d\b/i','<nobr>G-d</nobr>',$content);
  return $G_Dfilter;
}
add_filter('the_content', 'G_Dfilter');
add_filter('comment_text', 'G_Dfilter');

?>