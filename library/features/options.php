
<?php 
/*
Feature Name:   Menu
Developer URI:  http://www.DivTruth.com
Description:    All custom WP menu/page options
Version:        1.0
*/

/*****************************
 * Setup Custom Pages & Menus
 *****************************/
if(function_exists("register_options_page")){
    register_options_page('Theme Settings');
    register_options_page('Company Settings');
    register_options_page('Modules Settings');
    register_options_page('Documentation');    
}

/* ACTIONS */
add_action('admin_menu', 'div_option_page_menus');  # Div Truth Options Menu
add_action('admin_menu', 'hide_acf_admin_menu');  # Div Truth Options Menu

/* FUNCTIONS */
function hide_acf_admin_menu() {
    if(get_field('develop_mode','option') == "Disable"){
      remove_menu_page('edit.php?post_type=acf');
    }
}

function div_option_page_menus() {
  add_menu_page("<span class='DT_title'>Div Truth Options</span>", "<span class='DT_title'>Div Truth Options</span>", 'manage_options','admin.php?page=acf-options-company-settings',"",DIV_LIBRARY_URL.'/images/admin-logo.png',4);
  if(file_exists(THEME_FEATURES_DIR.'/documentation.php')){
    add_submenu_page('admin.php?page=acf-options-company-settings', __('Documentation','theme-main'), __('Documentation','theme-main'), 'manage_options', 'documentation', 'div_active_pages');
  }
  add_submenu_page('admin.php?page=acf-options-company-settings', __('Company Settings','theme-main'), __('Company Settings','theme-main'), 'manage_options', 'admin.php?page=acf-options-company-settings');
  add_submenu_page('admin.php?page=acf-options-company-settings', __('Theme Options','theme-main'), __('Theme Options','theme-main'), 'manage_options', 'admin.php?page=acf-options-theme-settings');      
  if(file_exists(THEME_FEATURES_DIR.'/modules.php')){
    add_submenu_page('admin.php?page=acf-options-company-settings', __('Modules','theme-main'), __('Modules','theme-main'), 'manage_options', 'admin.php?page=acf-options-modules-settings');
  }
}
function div_active_pages(){
	include(THEME_FEATURES_DIR.'/documentation.php');           #FEATURE: Documentation
}

?>