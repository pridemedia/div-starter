# [Div Starter 1.0](http://starter.divtruth.com/)

Div Starter is a development platform for [Wordpress Sites](http://wordpress.org/), a framework that aims to be powerful yet simple. Some of the greatness of frameworks like [Genesis](http://my.studiopress.com/themes/genesis/), but without the bulk and clutter; much like [Bones](http://themble.com/bones/) (actually based on bones with some powerful tools). 

Created and maintained by [Nick Worth](http://twitter.com/xtremefaith), forked from [Betsy Cohen's](https://twitter.com/betsela) [Plus1 Starter Theme](https://bitbucket.org/betsela/plus1-starter-theme).

## Requirements
+ **Requires**: 3.5.1 (WordPress)
+ **Tested up to**: 3.6
+ **License**: GPLv2 or later
+ **License URI**: http://www.gnu.org/licenses/gpl-2.0.html
+ **Tags**: custom post type, modules, features, bones, mobile first
+ **Features**: 
	+ [SASS](http://sass-lang.com/)
	+ [ACF](http://www.advancedcustomfields.com/) (child-level developed ACF fields and options)
	+ Theme Filter Options
		+ Dev Mode
		+ Execute Shortcodes in widgets
		+ Convert Twitter handlers to links
		+ Set JPG compression
	+ [Widgets](https://bitbucket.org/nworth/div-starter/src/dev/library/features/widgets?at=master)
		+ [Social Profile Widget](https://bitbucket.org/nworth/div-starter/src/dev/library/features/widgets/social-widget?at=master)
		+ [Content Widget](https://bitbucket.org/nworth/div-starter/src/dev/library/features/widgets/content-widget?at=master)
	+ [Shortcodes](https://bitbucket.org/nworth/div-starter/src/dev/library/features/shortcodes?at=master)
		+ [Content Feed](https://bitbucket.org/nworth/div-starter/src/dev/library/features/shortcodes/content_feed.php?at=master)

## Quick start

Developers simply setup your [typical wordpress site](http://teamtreehouse.com/library/websites/how-to-make-a-blog/getting-started-with-wordpress/the-famous-5minute-wordpress-install) (locally or on your server):

* Clone the starter repo: `git clone https://bitbucket.org/nworth/div-starter`.
* Clone the child repo: `git clone https://bitbucket.org/nworth/div-child`.
* Activate the Div Child Theme
	+ You can change the folder name to anything, suggested format: `div-project-slug`
	+ Update the child `style.css`, *always make sure the template matches the folder name of the starter theme*.
	+ Update the logo-login.png and any of the login.scss styles you want
+ Check out the documentation page: **Div Truth Options->Documentation** (more documentation to come)


## Bug tracker

Have a bug or a feature request? [Please open a new issue](https://bitbucket.org/nworth/div-truth-starter/issues?status=new&status=open). Before opening any issue, please search for existing issues.

## Contributors

**Nick Worth**

+ [Nick Worth](mailto:nick@positiveelement.com) - ([@Xtremefaith](http://twitter.com/Xtremefaith))
+ [https://bitbucket.org/nworth](https://bitbucket.org/nworth)

**Betsy Cohen**

+ [Betsy Cohen](mailto:betsy@positiveelement.com) - ([@Betsela](http://twitter.com/betsela))
+ [https://bitbucket.org/betsela](https://bitbucket.org/betsela)