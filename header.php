<!doctype html>  

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		
		<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
		
		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo THEME_LIBRARY_URL ?>/images/favicon.ico">
				
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<!--[if lt IE 9]>
			<script src="js/libs/html5shiv.js"></script>
		<![endif]-->
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		
	</head>
	
	<body <?php body_class(); ?> style="opacity:0;">
	
		<div id="container">
			
			<header class="header" role="banner">
			
				<div id="inner-header" class="wrap clearfix">
					<div id="header-top" class="clearfix">
						<div id="logo-wrap" class="eightcol first">
							<!-- to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> -->
							<p id="logo" class="h1"><a href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>
							<!-- if you'd like to use the site description you can un-comment it below -->
							<p id="desc"><?php bloginfo('description'); ?></p>
						</div>
						<div id="header-right" class="fourcol last">
							<nav role="navigation">
								<?php div_starter_utility_nav(); ?>
							</nav>
							<?php if ( is_active_sidebar( 'headerright' ) ) : ?>

								<?php dynamic_sidebar( 'headerright' ); ?>

							<?php else : ?>
							<?php endif; ?>
						</div>
					</div>
     
     				<div id="menu-icon-wrap"><div id="menu-icon"></div></div>
							
					<nav id="full" role="navigation">
						<?php div_starter_main_nav(); ?>
					</nav>

					<nav id="mobile" role="navigation">
						<?php div_starter_mobile_nav(); ?>
					</nav>
				
				</div> <!-- end #inner-header -->
			
			</header> <!-- end header -->